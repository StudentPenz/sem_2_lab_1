def swap():
    return None


def partition(lst, l, h):
    i = (l - 1)
    x = lst[h][1]

    for j in range(l, h):
        swap()
        if lst[j][1] <= x:
            i = i + 1
            lst[i], lst[j] = lst[j], lst[i]
    swap()
    lst[i + 1], lst[h] = lst[h], lst[i + 1]
    return i + 1


def bubble_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swapped = True
        while swapped:
            swapped = False
            for i in range(len(lst) - 1):
                swap()
                if lst[i] > lst[i + 1]:
                    lst[i], lst[i + 1] = lst[i + 1], lst[i]
                    swapped = True
        if reverse:
            return lst[::-1]
        else:
            return lst


def shell_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        t = len(lst) // 2
        while t > 0:
            for i in range(len(lst) - t):
                j = i
                swap()
                while j >= 0 and lst[j] > lst[j + t]:
                    lst[j], lst[j + t] = lst[j + t], lst[j]
                    j -= 1
            t = t // 2
        if reverse:
            return lst[::-1]
        else:
            return lst


def insert_sort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        for i in range(1, len(lst)):
            key = lst[i]
            j = i - 1
            while j >= 0 and lst[j] > key:
                swap()
                lst[j + 1] = lst[j]
                j -= 1
            lst[j + 1] = key
        if reverse:
            return lst[::-1]
        else:
            return lst


def quicksort(lst, reverse=False):
    if len(lst) <= 1:
        return lst
    else:
        swap()
        pivot = lst[len(lst)//2]
        l_nums = [n for n in lst if n < pivot]
        e_nums = [pivot] * lst.count(pivot)
        b_nums = [n for n in lst if n > pivot]
        lst = quicksort(l_nums) + e_nums + quicksort(b_nums)
        if reverse:
            return lst[::-1]
        else:
            return lst
