from src.app.sort_base import bubble_sort, insert_sort, quicksort, shell_sort
from math import log


def quadratic_complexity(array):
    return [e**2 for e in array]


def n_log_n_complexity(array):
    return [e*log(e, 2) for e in array]


FIRST = 10000
LAST = 100000
STEP = 10000
LEN_ARRAY_1 = [i for i in range(FIRST, LAST+1, STEP)]
LEN_ARRAY_time = [i for i in range(FIRST, LAST+1, STEP)]

name_algorithms = {
    '1': 'Пузырьковая сортировка',
    '2': 'Сортировка вставками',
    '3': 'Быстрая сортировка',
    '4': 'Сортировка Шелла'}

dict_algorithms = {
    '1': bubble_sort,
    '2': insert_sort,
    '3': quicksort,
    '4': shell_sort}


theoretical_complexity = {
    '1': quadratic_complexity,
    '2': quadratic_complexity,
    '3': n_log_n_complexity,
    '4': quadratic_complexity}









