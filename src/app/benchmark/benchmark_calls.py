from src.app.benchmark import benchmark_util
import matplotlib.pyplot as plt
from src.app.benchmark.bench_constants import FIRST, LAST, STEP, LEN_ARRAY_1, \
    dict_algorithms, name_algorithms, theoretical_complexity
from numpy import poly1d as np_poly1d, polyfit as np_polyfit


def calls(choice):
    for e in choice:
        fig = plt.figure()
        fig.set_size_inches(12, 9, forward=True)
        calls_lst = benchmark_util.lst_operation(dict_algorithms[e], [FIRST, LAST, STEP])
        plt.subplot()
        plt.title(f"Сложность({name_algorithms[e]})")
        trend = np_poly1d(np_polyfit(LEN_ARRAY_1, calls_lst, 1))
        plt.plot(LEN_ARRAY_1, theoretical_complexity[e](LEN_ARRAY_1), 'red',
                 label='Теоретическая сложность')
        plt.plot(LEN_ARRAY_1, trend(LEN_ARRAY_1), 'red', linestyle='--',
                 label='Эксперементальная сложность')
        plt.xlabel('длина массива, шт')
        plt.ylabel('Кол-во операций, шт')
        plt.legend()
        plt.savefig(f"graphs/{name_algorithms[e]}.png")


