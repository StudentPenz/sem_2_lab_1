from setuptools import setup, find_packages

setup(name='labs-app',
    version='0.1',
    description='Python example library',
    author='Me',
    author_email='me@.com',
    url='https://me.com',
    packages=find_packages(),
)