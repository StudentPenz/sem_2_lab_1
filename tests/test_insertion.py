import pytest
import src.app.sort_base as sort
import random


def test_ascending():

    test_lst = [random.randint(-100, 100) for _ in range(20)]
    theory = sorted(test_lst)
    practice = sort.insert_sort(test_lst)
    assert theory == list(practice)


def test_descending():

    test_lst = [random.randint(-100, 100) for _ in range(20)]
    theory = list(reversed(sorted(test_lst)))
    practice = sort.insert_sort(test_lst, reverse=True)
    assert theory == list(practice)


def test_input_data():

    test_lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(sort.insert_sort(test_lst))


def test_stability_ascending():

    test_lst = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    theory = sorted(test_lst)
    practice = sort.insert_sort(test_lst)
    assert theory == list(practice)


def test_stability_descending():

    test_lst = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
    theory = list(reversed(sorted(test_lst)))
    practice = sort.insert_sort(test_lst, reverse=True)
    assert theory == list(practice)